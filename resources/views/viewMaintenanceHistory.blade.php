@extends('layouts.app')

@section('content')
    <div class="container">

        @if(Session::has('success'))

            <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
        @endif
        <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome {{Auth::user()->name}}
                        <br> <span class="badge">{{Auth::user()->role}}</span>
                    </div>

                    <div class="panel-body">


                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Units</th>
                                <th>Department</th>
                                <th>Category</th>
                                <th>Last Maintenance</th>
                                <th>Next Maintenance</th>
                                <th>Supplier</th>
                                <th>Added By</th>
                            </tr>

                                <tr>
                                    <td>{{$equipment->name}}</td>
                                    <td>{{$equipment->description}}</td>
                                    <td>{{$equipment->units}}</td>
                                     <td>{{$equipment->dept}}</td>
                                    <td>{{$equipment->category}}</td>
                                    <td>
                                        @if(isset($equipment->lastMaintenance))
                                            {{$equipment->lastMaintenance}}
                                        @else
                                            Not yet maintained
                                        @endif
                                    </td>
                                    <td>{{$equipment->nextMaintenance}}</td>
                                    <td>{{$equipment->supplierName}}</td>
                                    <td>{{$equipment->Staff->name}}</td>

                                </tr>

                        </table>

                        <br>

                        <table class="table table-responsive">
                            <tr>
                                <th>S/N</th>
                                <th>Notes</th>
                                <th>Date</th>
                            </tr>

                            @if(isset($maintenance))
                                <?php $i = 1; ?>

                                @foreach($maintenance as $item)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <th>{{$item->notes}}</th>
                                        <th>{{$item->created_at}}</th>
                                    </tr>
                                    <?php $i++ ?>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="3">No maintenance records</td>
                                </tr>
                            @endif
                        </table>

                        <h3>Current Status: {{$equipment->status}}</h3>
                        <form method="post" class="form-inline" action="{{url('/change-status')}}">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$equipment->eid}}" name="eid">
                            <select style="width:200px;" class="form-control" name="status">
                                <option  class="form-control" >Available</option>
                                <option  class="form-control" >Missing</option>
                                <option  class="form-control" >Maintenance</option>
                                <option  class="form-control" >Broken</option>
                            </select>
                            <button type="submit" class="btn btn-primary right">Change Status</button>
                        </form>
                        <br><br>

                        <a href="{{url('/view-inventory')}}" class="btn color2 right">Go Back</a>
                        <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>
                        <a href="{{url('/add-maintenance-record/' . $equipment->eid)}}" class="btn btn-success right">Add Maintenance Record</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
