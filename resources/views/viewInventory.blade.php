@extends('layouts.app')

@section('content')
    <div class="container">

        @if(isset($status))

            @if($status == "Successfully Added Inventory")
                <div class="alert alert-success" align="center">{{$status}}</div>
            @else
                <div class="alert alert-danger" align="center">{{$status}}</div>
            @endif
        @endif
        <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>

            <div class="col-md-12" style="background-color: white">
                <div class="panel panel-default">
                    <div class="col-md-2">
                        <h3 align="center" >Categories</h3>
                        <ul style=" text-transform: uppercase; list-style-type: none;">
                            <li> <a href="{{url('/view-inventory')}}">All</a> </li>
                        @foreach($categories as $item)
                            <li>
                                <a href="{{url('/view-inventory/' . $item->category)}}">
                                     {{$item->category}}
                                </a>
                            </li>
                        @endforeach
                        </ul>
                    </div>

                    <div class="col-md-10">
                        <div class="panel-heading">Welcome {{Auth::user()->name}}
                            <br> <span class="badge">{{Auth::user()->role}}</span>

                          <h3 style="margin-top:-20px;"  align="center">
                           @if(isset($category))
                                {{$category}}
                                @else
                                All
                            @endif
                          </h3>
                        </div>

                        <div class="panel-body">


                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Units</th>
                                <th>Status</th>
                                <th>Department</th>
                                <th>Category</th>
                                <th>Added By</th>
                                <th>Last Maintenance</th>
                                <th>Next Maintenance</th>
                                <th></th>
                            </tr>

                            @foreach($equipments as $equipment)
                                <tr>
                                    <td>{{$equipment->name}}</td>
                                    <td>{{$equipment->description}}</td>
                                    <td>{{$equipment->units}}</td>
                                    <td>{{$equipment->status}}</td>
                                    <td>{{$equipment->dept}}</td>
                                    <td>{{$equipment->category}}</td>
                                    <td>{{$equipment->Staff->name}}</td>
                                    <td>
                                        @if(isset($equipment->lastMaintenance))
                                        {{$equipment->lastMaintenance}}
                                            @else
                                        Not yet maintained
                                        @endif
                                    </td>
                                    <td>{{$equipment->nextMaintenance}}</td>
                                    <td><a href="{{url('/view-maintenance-history/' . $equipment->eid )}}" class="btn color3 right">View History</a></td>
                                </tr>
                            @endforeach
                        </table>

                        <br>

                        <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>



                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
