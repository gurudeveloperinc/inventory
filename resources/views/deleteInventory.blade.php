@extends('layouts.app')

@section('content')
    <div class="container">

        @if(Session::has('success'))

                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif

        @if(Session::has('error'))
                <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
        @endif
        <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome {{Auth::user()->name}}
                        <br> <span class="badge">{{Auth::user()->role}}</span>
                    </div>

                    <div class="panel-body">


                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Units</th>
                                <th>Status</th>
                                <th>Department</th>
                                <th>Category</th>
                                <th>Added By</th>
                                <th></th>
                            </tr>

                            @foreach($equipments as $equipment)
                                <tr>
                                    <td>{{$equipment->name}}</td>
                                    <td>{{$equipment->description}}</td>
                                    <td>{{$equipment->units}}</td>
                                    <td>{{$equipment->status}}</td>
                                    <td>{{$equipment->dept}}</td>
                                    <td>{{$equipment->category}}</td>
                                    <td>{{$equipment->Staff->name}}</td>
                                    <td>
                                        <a href="{{url('/delete/'. $equipment->eid)}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                        <br>

                        <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
