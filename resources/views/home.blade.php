@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row home">
        <div class="logoStuff">
            <img src="{{url('/images/logo.png')}}" class="logo">
            <h3 class="logoHeader">Regent University Inventory Manager</h3>
        </div>
        @if(Auth::user()->role == "Admin")
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome {{Auth::user()->name}}
                   <br> <span class="badge">{{Auth::user()->role}}</span>
                </div>

                <div class="panel-body">
                    <div class="well color3 col-md-4">
                        <a href="{{url('/view-inventory')}}" style="color:white;">
                            {{count($equipments)}} total inventory
                        </a>
                    </div>


                    <div class="well color2 col-md-4">
                        <a href="{{url('/view-inventory?maintain=true')}}" style="color:white;">
                            {{count($maintain)}} need maintenance
                        </a>
                    </div>


                    <div class="well color1 col-md-4">
                        <a href="{{url('/view-inventory?recent=true')}}" style="color:white;">
                            {{count($recent)}} recently added
                        </a>
                    </div>
                    <a href="{{url('/view-inventory')}}" class="btn btn-primary">View Inventory</a>
                    <a href="{{url('/add-inventory')}}" class="btn btn-primary">Add Inventory</a>
                    <a href="{{url('/delete-inventory')}}" class="btn btn-danger">Delete Inventory</a>
                    <a href="{{url('/view-requests')}}" class="btn color2">View Requests</a>
                    <a href="{{url('/view-reports')}}" class="btn color3">View Reports</a>

                </div>
            </div>
        </div>
        @endif

        @if(Auth::user()->role == "Manager")
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome {{Auth::user()->name}}
                        <br> <span class="badge">{{Auth::user()->role}}</span>
                    </div>

                    <div class="panel-body">

                        <div class="well color3 col-md-4">
                            <a href="{{url('/view-inventory')}}" style="color:white;">
                            {{count($equipments)}} total inventory
                            </a>
                        </div>


                        <div class="well color2 col-md-4">
                            <a href="{{url('/view-inventory?maintain=true')}}" style="color:white;">
                            {{count($maintain)}} need maintenance
                            </a>
                        </div>


                        <div class="well color1 col-md-4">
                            <a href="{{url('/view-inventory?recent=true')}}" style="color:white;">
                            {{count($recent)}} recently added
                            </a>
                        </div>

                        <a href="{{url('/view-inventory')}}" class="btn btn-primary">View Inventory</a>
                        <a href="{{url('/view-requests')}}" class="btn btn-primary">View Requests</a>
                        <a href="{{url('/view-reports')}}" class="btn color3">View Reports</a>
                    </div>
                </div>
            </div>
        @endif

        @if(Auth::user()->role == "Department Head")
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome {{Auth::user()->name}}
                        <br> <span class="badge">{{Auth::user()->role}}</span>
                    </div>

                    <div class="panel-body">
                        <div class="well color3 col-md-4">
                            <a href="{{url('/view-inventory')}}" style="color:white;">
                                {{count($equipments)}} total inventory
                            </a>
                        </div>


                        <div class="well color2 col-md-4">
                            <a href="{{url('/view-inventory?maintain=true')}}" style="color:white;">
                                {{count($maintain)}} need maintenance
                            </a>
                        </div>


                        <div class="well color1 col-md-4">
                            <a href="{{url('/view-inventory?recent=true')}}" style="color:white;">
                                {{count($recent)}} total requests
                            </a>
                        </div>
                        <a href="{{url('/view-inventory')}}" class="btn btn-primary">View Inventory</a>
                        <a href="{{url('/view-requests')}}" class="btn btn-primary">View Requests</a>
                        <a href="{{url('/add-request')}}" class="btn color2">Add Request</a>
                    </div>
                </div>
            </div>

        @endif

    </div>
</div>
@endsection
