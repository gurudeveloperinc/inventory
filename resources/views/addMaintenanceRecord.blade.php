@extends('layouts.app')

@section('content')
    <div class="container">

        @if(isset($status))

            @if($status == "Successfully Added Inventory")
                <div class="alert alert-success" align="center">{{$status}}</div>
            @else
                <div class="alert alert-danger" align="center">{{$status}}</div>
            @endif
        @endif
        <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Add maintenance record for  {{$equipment->name}}</h3>
                    </div>

                    <div class="panel-body">


                        <form method="post" action="{{url('/add-maintenance-record')}}">
                            {{csrf_field()}}

                            <input type="hidden" name="eid" value="{{$equipment->eid}}">
                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea class="form-control" id="notes" name="notes"></textarea>
                            </div>

                            <button class="btn btn-primary" type="submit">Add Record</button>
                            <button class="btn btn-warning" type="reset">Clear</button>

                        </form>

                        <br>
                        <a href="{{url('/view-maintenance-history/' . $equipment->eid)}}" class="btn color2 right">Go Back</a>

                        <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
