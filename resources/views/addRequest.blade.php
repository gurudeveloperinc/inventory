@extends('layouts.app')

@section('content')
    <div class="container">


        @if( Session::has('success') )
            <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif


        @if( Session::has('error') )
            <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
        @endif

            <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Add request</h3>
                    </div>

                    <div class="panel-body">


                        <form method="post" action="{{url('/add-request')}}">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea class="form-control" id="notes" name="content"></textarea>
                            </div>

                            <button class="btn btn-primary" type="submit">Submit Request</button>
                            <button class="btn btn-warning" type="reset">Clear</button>

                        </form>

                        <br>

                        <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
