@extends('layouts.app')

@section('content')
    <div class="container">
<?php use Carbon\Carbon; ?>
        @if( Session::has('success') )
            <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif


        @if( Session::has('error') )
            <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
        @endif

        <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>

            <div class="col-md-12" style="background-color: white">
                <div class="panel panel-default">

                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel-heading">Welcome {{Auth::user()->name}}
                            <br> <span class="badge">{{Auth::user()->role}}</span>

                            <h3 style="margin-top:-20px;"  align="center">
                                All Requests
                            </h3>
                        </div>

                        <div class="panel-body">


                            <table class="table table-hover">
                                <tr>
                                    <th>Content</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Requested By</th>
                                    <th></th>
                                </tr>

                                @foreach($requests as $item)
                                    <tr>
                                        <td>{{$item->content}}</td>
                                        <td>{{$item->status}}</td>
                                        <td>{{  Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->toFormattedDateString() }}</td>
                                        <td>{{$item->User->name}} <small style="color:black;">({{$item->User->staffid}})</small></td>
                                        <td>
                                            @if($item->status == "Pending" && Auth::user()->role == "Admin")
                                            <a href="{{url('/approve/' . $item->rid )}}" class="btn btn-primary">Approve</a>
                                            <a href="{{url('/reject/' . $item->rid )}}" class="btn btn-danger">Reject</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                            <br>

                            <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
