@extends('layouts.app')

@section('content')
    <div class="container">

        @if(isset($status))

            @if($status == "Successfully Added Inventory")
                <div class="alert alert-success" align="center">{{$status}}</div>
                @else
                <div class="alert alert-danger" align="center">{{$status}}</div>
            @endif
        @endif
        <div class="row home">
            <div class="logoStuff">
                <img src="{{url('/images/logo.png')}}" class="logo">
                <h3 class="logoHeader">Regent University Inventory Manager</h3>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome {{Auth::user()->name}}
                        <br> <span class="badge">{{Auth::user()->role}}</span>
                    </div>

                    <div class="panel-body">

                        <form method="post" action="{{url('/add-inventory')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="ename">Equipment Name</label>
                                <input type="text" class="form-control" id="ename" placeholder="Eg - Printer" name="ename">
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="sup">Supplier Name</label>
                                <input type="text" class="form-control" id="sup" placeholder="Eg - James Holdings" name="supplierName">
                            </div>


                            <div class="form-group">
                                <label for="units">Units</label>
                                <input type="number" class="form-control" id="units" name="units">
                            </div>

                            <div class="form-group">
                                <label for="dept">Department</label>
                                <select name="dept" class="form-control">
                                    <option>Informatics</option>
                                    <option>Engineering</option>
                                    <option>Accounting</option>
                                    <option>Management</option>
                                    <option>Psychology</option>
                                    <option>Theology</option>
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" id="category" name="category">
                            </div>

                            <select class="form-control" name="status">
                                <option>Available</option>
                                <option>Missing</option>
                                <option>Maintenance</option>
                                <option>Broken</option>
                            </select>

                            <br>

                            <div class="form-group">
                                <label for="category">Maintenance Schedule in days</label>
                                <input type="number" class="form-control" id="category" name="schedule">
                            </div>


                            <button class="btn btn-primary" type="submit">Add Inventory</button>
                            <button class="btn btn-warning" type="reset">Clear</button>

                            </form>

                        <br>

                        <a href="{{url('/')}}" class="btn color3 right">Go to Dashboard</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
