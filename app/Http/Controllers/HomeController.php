<?php

namespace App\Http\Controllers;

use App\equipment;
use App\maintenance;
use App\requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $equipments = equipment::all();
	    $recent = equipment::where('created_at', ">" , Carbon::now()->subWeek(2) )->get();
		$maintain = equipment::where('nextMaintenance', "<", Carbon::now())->get();

        return view('home',[
	        'equipments' => $equipments,
	        'recent' => $recent,
	        'maintain' => $maintain
        ]);
    }

	public function viewReports() {
		$categories = equipment::all()->unique('status');
		$status = Input::get('status');

		if(isset($status) && !empty($status)){
			$equipments = equipment::where('status', $status)->get();
		} else{
			$equipments = equipment::where('status', "Broken")->get();
		}

		return view('viewReports',[
			'equipments' => $equipments,
			'categories' => $categories
		]);
	}

	public function getAddRequest() {
		return view('addRequest');
	}

	public function getAddInventory() {
		return view('addInventory');
	}

	public function viewRequests() {
		$requests = requests::all()->sortByDesc("created_at");

		return view('viewRequests',[
			'requests' => $requests
		]);
	}

	public function getViewInventory() {
		$equipments = equipment::all()->sortByDesc('created_at');
		$categories = equipment::all()->unique('category');

		$recent = Input::get('recent');

		if(isset($recent) && !empty($recent)){
			$equipments = equipment::where('created_at', Carbon::now()->subWeek(2) );
		}

		$needMaintenance = Input::get("maintain");

		if(isset($needMaintenance) && !empty($needMaintenance)){
			$equipments = equipment::all()->where('nextMaintenance', "<", Carbon::now());
		}



		return view('viewInventory',[
			'equipments' => $equipments,
			'categories' => $categories
		]);
	}

	public function viewInventoryByCategory( $category ) {

		$equipments = equipment::all()->where('category',$category)->sortByDesc('created_at');
		$categories = equipment::all()->unique('category');

		return view('viewInventory',[
			'equipments' => $equipments,
			'categories' => $categories,
			'category' => $category
		]);
	}

	public function getDeleteInventory(){

		$equipments = equipment::all()->sortByDesc('created_at');

		return view('deleteInventory',[
			'equipments' => $equipments
		]);
	}

	public function getViewMaintenanceHistory($eid) {
		$equipment = equipment::find($eid);

		$maintenance = maintenance::where('eid',$eid)->get();
		return view('viewMaintenanceHistory',[
			'equipment' => $equipment,
			'maintenance' => $maintenance
		]);
	}

	public function getAddMaintenanceRecord($eid){

		$equipment = equipment::find($eid);

		return view('addMaintenanceRecord',['equipment' => $equipment]);
	}

	public function postAddMaintenanceRecord( Request $request ) {

		$maintenance = new maintenance();
		$maintenance->eid = $request->input('eid');
		$maintenance->notes = $request->input('notes');
		$maintenance->save();

		$equipment = equipment::find($request->input('eid') );
		$equipment->lastMaintenance = Carbon::now();
		$days = $equipment->schedule;
		$equipment->nextMaintenance = Carbon::now()->addDays($days);
		$equipment->save();

		$request->session()->flash("success", "Successfully Added Record");

		return redirect('/view-maintenance-history/' . $equipment->eid);
	}

	public function postAddInventory( Request $request ) {

		$equipment = new equipment();
		$equipment->name = $request->input('ename');
		$equipment->description = $request->input('description');
		$equipment->units = $request->input('units');
		$equipment->status = $request->input('status');
		$equipment->supplierName = $request->input('supplierName');
		$equipment->dept = $request->input('dept');
		$equipment->category = $request->input('category');
		$equipment->schedule = $request->input('schedule');
		$equipment->nextMaintenance = Carbon::now()->addDays($request->input('schedule'));
		$equipment->uid = Auth::user()->uid;
		$status = $equipment->save();

		if($status) return view('addInventory',['status' => "Successfully Added Inventory"]);
		else return view('addInventory',['status' => "Error Adding Inventory"]);
	}

	public function postChangeStatus(Request $request){
		$equipment = equipment::find($request->input('eid'));
		$equipment->status = $request->input('status');
		$equipment->save();
		$request->session()->flash('success', 'Status Changed Successfully');
		return redirect('/view-maintenance-history/' . $equipment->eid);
	}

	public function deleteInventory( $eid ) {
		$equipment = equipment::find($eid);
		$equipment->delete();

		Session::flash('success',"Equipment Deleted");
		return redirect('/delete-inventory');
	}

	public function postAddRequest( Request $request ) {
		$content = $request->input('content');
		$newRequest = new requests();
		$newRequest->content = $content;
		$newRequest->uid = Auth::user()->uid;
		$status = $newRequest->save();

		if($status) $request->session()->flash("success", "Request Added Successfully");
		else $request->session()->flash("error", "Sorry an error occurred");

		return redirect('/add-request');
	}

	public function approveRequest(Request $request, $rid ) {
		$req = requests::find($rid);
		$req->status = "Approved";
		$req->save();

		$request->session()->flash("success","Request Approved");
		return redirect('/view-requests');
	}

	public function rejectRequest(Request $request, $rid ) {
		$req = requests::find($rid);
		$req->status = "Rejected";
		$req->save();

		$request->session()->flash("success","Request Rejected");
		return redirect('/view-requests');
	}


}
