<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class equipment extends Model
{
    protected $table = 'equipments';
	protected $primaryKey = 'eid';

	public function Staff(){
		return $this->hasOne('App\User','uid','uid');
	}
}
