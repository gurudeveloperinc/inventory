<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requests extends Model
{
    protected $primaryKey = "rid";
	public $table = "requests";

	public function User(){
		return $this->belongsTo('App\User','uid','uid');
	}
}
