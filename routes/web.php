<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/add-inventory','HomeController@getAddInventory');
Route::get('/view-inventory','HomeController@getViewInventory');
Route::get('/view-inventory/{category}','HomeController@viewInventoryByCategory');
Route::get('/delete-inventory','HomeController@getDeleteInventory');
Route::get('/delete/{eid}','HomeController@deleteInventory');
Route::get('/view-maintenance-history/{eid}','HomeController@getViewMaintenanceHistory');
Route::get('/add-maintenance-record/{eid}','HomeController@getAddMaintenanceRecord');
Route::get('/view-reports','HomeController@viewReports');
Route::get('/view-requests','HomeController@viewRequests');
Route::get('/add-request','HomeController@getAddRequest');

Route::get('/approve/{rid}','HomeController@approveRequest');
Route::get('/reject/{rid}','HomeController@rejectRequest');



Route::post('/add-inventory','HomeController@postAddInventory');
Route::post('/change-status','HomeController@postChangeStatus');
Route::post('/add-maintenance-record','HomeController@postAddMaintenanceRecord');
Route::post('/add-request','HomeController@postAddRequest');
